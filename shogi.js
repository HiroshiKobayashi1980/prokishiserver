'use strict'
const dataTable = require('./data_table.js');

//指定したグリッドから別の指定したグリッドに移動できるかを返します。
exports.isMovableFromBoard = function(board, fromX, fromY, toX, toY) {
	let movableTable = getMovablesFromBoard(board, fromX, fromY);
	let to = getIndex(toX, toY);
	return movableTable[to] > -1;
}

//持駒から別の指定したグリッドに移動できるかを返します。
exports.isMovableFromHand = function(board, player, index, toX, toY) {
	let movableTable = getMovablesFromHand(board, index);
	let to = getIndex(toX, toY);
	return movableTable[to] > -1;
}

//成りの判定をおこないます。
exports.isPromotion = function(board, fromX, fromY, toY) {
	switch (getPiece(board, fromX, fromY)) {
	case 1:case 2: case 4:
		return less(3, 0, toY);
	case 5:
		return less(3, 2, toY);
	case 6: case 7:
		return less(3, 1, toY);
	case 21: case 22: case 24:
		return greater(6, 9, toY);
	case 25:
		return greater(6, 7, toY);
	case 26: case 27:
		return greater(6, 8, toY);
	default:
		return false;
	}
}

//自分の番かどうかを返す
exports.isInitiative = function(board, x, y) {
    let owner = getOwner(board, x, y);
    return board.initiative == owner;
}

//指定した座標が空欄かどうかを返します。
exports.isEmpty = function(board, x, y) {
    return board.position_table[getIndex(x, y)] == 40;
}

//棋譜を出力
exports.getText = function(board, toX, toY) {
	let owner = getOwner(board, toX, toY);
    let symbol = owner == 0 ? '▲' : owner == 1 ? '△' : '';
	let piece = dataTable.PIECE_LABELS[getPiece(board, toX, toY)];
	return symbol + toX + dataTable.NUMBERS[toY] + piece;
}

exports.getMovablesFromBoard = function(board, fromX, fromY) {
    getMovablesFromBoard(board, fromX, fromY);
}

exports.getMovablesFromHand = function(board, piece) {
    getMovablesFromHand(board, piece);
}

//グリッドの座標から駒の番号を返します。
function getIndex(x, y) {
	return (y - 1) * 9 + 9 - x;
}

//インデックスからX座標を返します。
function getX(index) {
	return 9 - index % 9;
}

//インデックスからY座標を返します。
function getY(index) {
	return index / 9 + 1;
}

//インデックスで指定した持駒を取得
function getHandPiece(board, player, index) {
	return board.hand_table[player][index];
}

//二歩にならない列ならtrueを返す。
function is2Pawn(board, line, piece) {
	let pawnCount = 0;
	if (piece == 7 || piece == 27) {
		for (let i = 1; i < 9; i++) {
			if (board.position_table[getIndex(line, i)] == piece) pawnCount++;
		}
	}
	return pawnCount > 1 ? true : false;
}

//グリッドの座標から駒の種類を返します。
function getPiece(board, x, y) {
	return board.position_table[getIndex(x, y)];
}

//指定したグリッドを所持するプレイヤー番号を返します。いなければ0を返す。
function getOwner(board, x, y){
	let piece = getPiece(board, x, y);
	if (piece < 20) return 0;
	if (piece < 40) return 1;
	return -1;
}

//y座標の位置から呼び出すイベントを判別
function less(value1, value2, y) {
	if (y <= value1) {
		if (y <= value2) return true;
		return true;
	}
	return false;
}

//y座標の位置から呼び出すイベントを判別
function greater(value1, value2, y) {
	if (y > value1) {
		if (y > value2) return true;
		return true;
	}
	return false;
}

//移動できるグリッドを配列に格納します。
function getMovablesFromBoard(board, fromX, fromY) {
	let piece = getPiece(board, fromX, fromY);
    let arrayX = [];
    let arrayY = [];
	switch (piece) {
	case 7: case 27:
		arrayX = dataTable.MOVABLE07X;
		arrayY = dataTable.MOVABLE07Y;
		break;
	case 6: case 26:
		arrayX = dataTable.MOVABLE06X;
		arrayY = dataTable.MOVABLE06Y;
		break;
	case 5: case 25:
		arrayX = dataTable.MOVABLE05X;
		arrayY = dataTable.MOVABLE05Y;
		break;
	case 4: case 24:
		arrayX = dataTable.MOVABLE04X;
		arrayY = dataTable.MOVABLE04Y;
		break;
	case 3: case 14:case 15: case 16: case 17: case 23: case 34: case 35: case 36: case 37:
		arrayX = dataTable.MOVABLE03X;
		arrayY = dataTable.MOVABLE03Y;
		break;
	case 2: case 22:
		arrayX = dataTable.MOVABLE02X;
		arrayY = dataTable.MOVABLE02Y;
		break;
	case 12: case 32:
		arrayX = dataTable.MOVABLE12X;
		arrayY = dataTable.MOVABLE12Y;
		break;
	case 1: case 21:
		arrayX = dataTable.MOVABLE01X;
		arrayY = dataTable.MOVABLE01Y;
		break;
	case 11: case 31:
		arrayX = dataTable.MOVABLE11X;
		arrayY = dataTable.MOVABLE11Y;
		break;
	case 0: case 20:
		arrayX = dataTable.MOVABLE00X;
		arrayY = dataTable.MOVABLE00Y;
		break;
	}
	return setMovableGrid(board, fromX, fromY, arrayX, arrayY);
}

//移動できるグリッドに数字を入力
function setMovableGrid(board, fromX, fromY, arrayX, arrayY) {
	let x = 0;
	let y = 0;
    let movableTable = createMovableTable();
	for (let i = 0; i < arrayX.length; i++) {
		for (let j = 0; j < arrayX[i].length; j++) {
			if (getOwner(board, fromX, fromY) == 0) {
				x = Number(fromX) + arrayX[i][j];
				y = Number(fromY) + arrayY[i][j];
			}
			else if (getOwner(board, fromX, fromY) == 1) {
				x = Number(fromX) + arrayX[i][j] * -1;
				y = Number(fromY) + arrayY[i][j] * -1;
			}
			if (x > 0 && y > 0 && x < 10 && y < 10) {
				let target = getPiece(board, x, y);
				if (i > 0) j = target != 40 ? 8 : j;
				let index = getIndex(x, y);
				if (getOwner(board, fromX, fromY) != getOwner(board, x, y)) {
					movableTable[index] = dataTable.PRIORITY_LIST[getPiece(board, x, y)];
				}
			}
		}
	}
    return movableTable;
}

//持駒から移動できるグリッドを配列に格納します。
function getMovablesFromHand(board, piece) {
	let start = 1;
	let end = 10;
    let movableTable = createMovableTable();
	switch (piece) {
	case 6: case 7:
		start = 2;
		break;
	case 26: case 27:
		end = 9;
		break;
	case 5:
		start = 3;
		break;
	case 25:
		end = 8;
		break;
	default:
		break;
	}
	for (let i = 1; i < 10; i++) {
		if (is2Pawn(i, piece)) {
			for (let j = start; j < end; j++) {
				if (board.position_table[get_index(i, j)] == 40) {
					movableTable[get_index(i, j)] = 0;
			    }
			}
		}
	}
    return movableTable;
}

//移動できるグリッドを-1に戻す
function createMovableTable() {
    let movableTable = [];
	for (let i = 0; i < 81; i++) {
		movableTable.push(-1);
	}
	return movableTable;
}
