'use strict';
const request = require('request');

switch(process.argv[2]) {
    case 'new_game':
        newGame();
        break;
    case 'play_from_board':
        playFromBoard();
        break;
    case 'play_from_hand':
        playFromHand();
        break;
}

function newGame() {
    let url = 'http://192.168.1.103:8000/shogi/new_game';
    let options = {
        method: 'GET',
        url: url,
    };
    httpRequest(options, '対局開始');
};

function playFromBoard() {
    let url = 'http://192.168.1.103:8000/shogi/play_from_board?id=ARGV3&from_x=ARGV4&from_y=ARGV5&to_x=ARGV6&to_y=ARGV7&is_promotion=ARGV8';
    url = setArgv(url, 3);
    let options = {
        method: 'GET',
        url: url,
    };
    httpRequest(options, '手の送信');
};

function playFromHand() {
    let url = 'http://192.168.1.103:8000/shogi/play_from_hand?id=ARGV3&player=ARGV4&index=ARGV5&to_x=ARGV6&to_y=ARGV7';
    url = setArgv(url, 3);
    let options = {
        method: 'GET',
        url: url,
    };
    httpRequest(options, '手の送信');
};

function setArgv(url, start) {
    for (let i = start; i < process.argv.length; i++) {
        url = url.replace('ARGV' + i, process.argv[i]);
    }
    return url;
}

function httpRequest(options, work) {
    if (options.method == 'GET') {
        request.get(options, work, function(error, response, body) {
            showResponse(work, error, response, body);
        });
    } else if (options.method == 'POST') {
        request.post(options, work, function(error, response, body) {
            showResponse(work, error, response, body);
        });
    }
}

function showResponse(work, error, response, body) {
    if (error) {
        console.log(work + 'に失敗しました！ error: ' + error);
    } else if (response.statusCode == 200 || response.statusCode == 202) {
        console.log(body);
    } else {
        console.log(work + 'に失敗しました！ statusCode: ' + response.statusCode);
    }
}
