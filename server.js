'use strict';
const http = require('http');
const fs = require('fs');
const shogi = require('./shogi.js');
const url = require('url');
const uuid = require('uuid');
const dataTable = require('./data_table.js');

//サーバ起動
http.createServer(function(req, res) {
    if (req.method == 'GET') {
        let urlObj = url.parse(req.url, true);
        let pathname = urlObj.pathname;
        let query = urlObj.query;
        switch(pathname) {
            case '/shogi/new_game':
                newGame(function(result) {
                    response(res, 'text/plain', result);
                });
                break;
            case '/shogi/delete_game':
                deleteGame(function(result) {
                    response(res, 'text/plain', result);
                });
                break;
            case '/shogi/play_from_board':
                readBoard(query.id, function(board) {
                    playFromBoard(board, query, function(result) {
                        writeBoard(board, query.id, function() {
                            response(res, 'text/plain', result);
                        });
                    });
                });
                break;
            case '/shogi/play_from_hand':
                readBoard(query.id, function(board) {
                    playFromHand(board, query, function(result) {
                        writeBoard(board, query.id, function() {
                            response(res, 'text/plain', result);
                        });
                    });
                });
                break;
            case '/shogi/get_board':
                readBoard(query.id, function(board) {
                    response(res, 'application/json', board);
                });
                break;
            case '/shogi/get_movables_from_board':
                readBoard(query.id, function(board) {
                    getMovablesFromBoard(board, query, function(result) {
                        response(res, 'application/json', result);
                    });
                });
                break;
            case '/shogi/get_movables_from_hand':
                readBoard(query.id, function(board) {
                    getMovablesFromHand(board, query, function(result) {
                        response(res, 'application/json', result);
                    });
                });
                break;
        }
    }
}).listen(8000);

//レスポンス送信
function response(res, contentsType, result) {
    res.writeHead(200, {'Content-Type': contentsType});
    res.end(result);
}

//盤面を読み込む
function readBoard(id, callback) {
    fs.readFile('game_board/' + id + '.json', 'utf-8', function(error, text) {
        let board = JSON.parse(text);
        showBoard(board);
        callback(board);
    });
}

//盤面に書き込む
function writeBoard(board, id, callback) {
    fs.writeFile('game_board/' + id + '.json', JSON.stringify(board, null, '    '), function() {
        showBoard(board);
        callback();
    });
}

//新しくゲームを開始してIDを割り当て
function newGame(callback) {
    let id = uuid.v4();
    fs.createReadStream('game_board.tpl').pipe(fs.createWriteStream('game_board/' + id + '.json'));
    callback(id);
}

//ゲームを破棄する
function deleteGame(id, callback) {
    fs.unlink('game_board/' + id + '.json', function() {
        callback('OK');
    });
}

//盤から指す
function playFromBoard(board, query, callback) {
    if (shogi.isInitiative(board, query.from_x, query.from_y)) {
        if (shogi.isEmpty(board, query.from_x, query.from_y)) {
            callback('駒がありません！');
        } else {
            if (shogi.isMovableFromBoard(board, query.from_x, query.from_y, query.to_x, query.to_y)) {
                if (shogi.isPromotion(board, query.from_x, query.from_y, query.to_y)) {
                    if (query.is_promotion && query.is_promotion == 1) {
                        boardToBoard(board, query.from_x, query.from_y, query.to_x, query.to_y);
                        endTurn(board);
                        callback(shogi.getText(board, query.to_x, query.to_y));
                    } else {
                        callback('成りますか？');
                    }
                } else {
                    boardToBoard(board, query.from_x, query.from_y, query.to_x, query.to_y);
                    endTurn(board);
                    callback(shogi.getText(board, query.to_x, query.to_y));
                }
            } else {
                callback('そこには置けません！'); 
            }
        }
    } else {
        callback('相手の番です！');
    }
}

//持駒から指す
function playFromHand(board, query, callback) {
    if (board.initiative == query.player) {
        if (board.hand_table[query.player][query.index] == 40) {
            callback('駒がありません！');
        } else {
            if (shogi.isMovableFromHand(query.hand, query.to_x, query.to_y)) {
                handToBoard(board, query.player, query.index, query.to_x, query.to_y);
                endTurn(board);
                callback(shogi.getText(board, query.to_x, query.to_y));
            } else {
                callback('そこには置けません！'); 
            }
        }
    } else {
        callback('相手の番です！');
    }
}

//指定した盤上の駒が動かせるグリッドを取得
function getMovablesFromBoard(board, query, callback) {
    let array = shogi.getMovablesFromBoard(board, query.from_x, query.from_y);
    callback(array);
}

//指定した持駒が動かせるグリッドを取得
function getMovablesFromHand(board, query, callback) {
    let array = shogi.getMovablesFromHand(board, query.piece);
    callback(array);
}

//座標からインデックスを取得
function getIndex(positionX, positionY) {
	return  (positionY - 1) * 9 + 9 - positionX;
}

//座標から駒を取得
function getPiece(board, positionX, positionY) {
	return board.position_table[getIndex(positionX, positionY)];
}

//持駒から盤へ移動
function handToBoard(board, player, hand, to_x, to_y) {
    let active = -1;
    active = board.hand_table[player][data1.from_x - 10];
    board.hand_table[player][data1.from_x - 10] = 40;
    let toPos = getIndex(toX, toY);
    let passive = getPiece(board, toX, toY);
    board.position_table[fromPos] = 40;
    board.position_table[toPos] = active;
    if (passive != 40) boardToHand(board, passive);
}

//盤から盤へ移動
function boardToBoard(board, fromX, fromY, toX, toY) {
    let fromPos = getIndex(fromX, fromY);
    let toPos = getIndex(toX, toY);
    let active = getPiece(board, fromX, fromY);
    let passive = getPiece(board, toX, toY);
    board.position_table[fromPos] = 40;
    board.position_table[toPos] = active;
    if (passive != 40) boardToHand(board, passive);
}

//盤から持駒へ移動
function boardToHand(board, piece) {
    let owner = piece < 20 ? 0 : 1;
    for (let i = 0; i < board.hand_table[owner].length; i++) {
        if (board.hand_table[owner][i] == 40) {
            board.hand_table[owner][i] = piece;
            break;
        }
    }
}

//盤面を表示
function showBoard(board) {
    let result = board.turn + '手目\n\n' + showHand(board, 0) + '\n 9 8 7 6 5 4 3 2 1\n';
    for (let i = 1; i < 10; i++) {
        for (let j = 1; j < 10; j++) {
            result += dataTable.PIECE_LABELS[board.position_table[(i - 1) * 9 + (j - 1)]];
        }
        result += ' ' + dataTable.NUMBERS[i] + '\n';
    }
    result += '\n' + showHand(board, 1);
    console.log(result);
}

//持駒を表示
function showHand(board, player) {
    let result = player == 0 ? '▲持駒\n' : player == 1 ? '△持駒\n' : '';
    for (let i = 0; i < 38; i++) {
        result += (i + 1) + ': ' + dataTable.PIECE_LABELS[board.hand_table[player][i]] + ' ';
    }
    return result + '\n';
}

//自分の番を終了します
function endTurn(board) {
    let initiative = board.initiative == 0 ? 1 : 0; 
    board.initiative = initiative;
    board.turn++;
}
